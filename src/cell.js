import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';



export class Cell extends React.Component {
  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      curNumber: (this.props.val == 0) ? '' : this.props.val,
      cellType: ((this.props.val == 0) ? 'userEnteredVal' : 'defaultValue'),
      originialNum: (this.props.val == 0) ? '' : this.props.val,
      };
  }

  handleChange(event) {
    var curVal = event.target.value
    if (this.state.cellType == 'defaultValue')
      return
    if (curVal == '' || ((curVal > 0) && (curVal < 10))){
      this.setState({curNumber: curVal});
    }
    }

    handleBlur(event) {
      var curVal = event.target.value
      var nextInfo =  this.getNextCellTypeAndVal(curVal)

      this.setState({curNumber: nextInfo.value,
              cellType: nextInfo.type});
    }

    getNextCellTypeAndVal(curVal){
      if ((curVal < 0) || (curVal > 10)){
        return {type: this.state.cellType,
            value: this.state.originialNum};
      }
      else if (this.state.cellType == 'defaultValue'){
          return {type: 'defaultValue',
            value: this.state.originialNum};
      }
      else if (this.state.cellType == 'userEnteredVal'){
        return {type: 'userEnteredVal',
            value: curVal};
      }
    }

    handleClick(){
    this.setState({cellType: 'userEnteredVal',
              curNumber: ''});
  }

  render(){
    const curNumber = this.state.curNumber
    const cellType = this.state.cellType
    return <input type="text" value={curNumber} onChange={this.handleChange} onBlur={this.handleBlur} className={cellType} />;
  } 
}