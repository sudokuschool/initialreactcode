import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Cell} from './cell.js';

ReactDOM.render(<Cell val="5"/>, document.getElementById('root'));